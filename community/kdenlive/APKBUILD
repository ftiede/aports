# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kdenlive
pkgver=22.12.0
pkgrel=0
# ppc64le mlt uses 64bit long double while libgcc uses 128bit long double
# ppc64le, s390x and riscv64 blocked by qt5-qtwebengine -> purpose
arch="all !armhf !ppc64le !s390x !riscv64"
url="https://kdenlive.org"
pkgdesc="An intuitive and powerful multi-track video editor, including most recent video technologies"
license="GPL-2.0-or-later"
depends="
	ffmpeg
	frei0r-plugins
	qt5-qtquickcontrols
	"
makedepends="
	extra-cmake-modules
	kdeclarative-dev
	kdoctools-dev
	kfilemetadata-dev
	knewstuff-dev
	knotifyconfig-dev
	kxmlgui-dev
	mlt-dev
	purpose-dev
	qt5-qtbase-dev
	qt5-qtmultimedia-dev
	qt5-qtnetworkauth-dev
	qt5-qtsvg-dev
	rttr-dev
	samurai
	"
checkdepends="xvfb-run"
source="https://download.kde.org/stable/release-service/$pkgver/src/kdenlive-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"
options="!check" # Broken

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
a847ae48db7efdf6bdf8fbb54f9e3ae9af5e11ae499a9f8a77d06a5fca24da93cb9314395d79a44a5beb553cc22a4ee681fcaa4df231f9290758f4e55b06efea  kdenlive-22.12.0.tar.xz
"
