# Contributor: Luca Weiss <luca@z3ntu.xyz>
# Maintainer: psykose <alice@ayaya.dev>
pkgname=pyside6
pkgver=6.4.1
pkgrel=0
pkgdesc="Enables the use of Qt6 APIs in Python applications"
url="https://doc.qt.io/qtforpython-6/"
arch="all"
license="LGPL-3.0-only AND GPL-2.0-only"
makedepends="
	clang-dev
	clang-static
	cmake
	llvm-dev
	llvm-static
	py3-numpy-dev
	py3-setuptools
	py3-wheel
	python3-dev
	qt6-qt3d-dev
	qt6-qtbase-dev
	qt6-qtcharts-dev
	qt6-qtconnectivity-dev
	qt6-qtdatavis3d-dev
	qt6-qtdeclarative-dev
	qt6-qthttpserver-dev
	qt6-qtmultimedia-dev
	qt6-qtnetworkauth-dev
	qt6-qtpositioning-dev
	qt6-qtquick3d-dev
	qt6-qtremoteobjects-dev
	qt6-qtscxml-dev
	qt6-qtsensors-dev
	qt6-qtserialport-dev
	qt6-qtspeech-dev
	qt6-qtsvg-dev
	qt6-qttools-dev
	qt6-qtwebchannel-dev
	qt6-qtwebsockets-dev
	samurai
	"
subpackages="
	py3-pyside6:pyside
	py3-shiboken6:pyshiboken
	$pkgname-dev
	"
source="$pkgver-$pkgname-.tar.xz::https://download.qt.io/official_releases/QtForPython/pyside6/PySide6-$pkgver-src/pyside-setup-opensource-src-$pkgver.tar.xz
	$pkgname-fix-python-1.patch::https://code.qt.io/cgit/pyside/pyside-setup.git/patch/?id=34e8eb569b865259055558384f91b7eeb4e27958
	$pkgname-fix-python-2.patch::https://code.qt.io/cgit/pyside/pyside-setup.git/patch/?id=59f159de51345d122638e16ad395cb00433c0d04
	"
builddir="$srcdir/pyside-setup-opensource-src-$pkgver"
options="!check" # Tests fail

build() {
	export SETUPTOOLS_USE_DISTUTILS=stdlib
	CFLAGS="$CFLAGS -flto=auto" \
	CXXFLAGS="$CXXFLAGS -flto=auto" \
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_SKIP_RPATH=ON \
		-DBUILD_SHARED_LIBS=True \
		-DPYTHON_EXECUTABLE=/usr/bin/python3 \
		-DBUILD_TESTS=OFF

	PYTHONPATH="$PWD/build/sources" \
	cmake --build build
}

package() {
	export SETUPTOOLS_USE_DISTUTILS=stdlib
	DESTDIR="$pkgdir" cmake --install build/sources/pyside6
	DESTDIR="$pkgdir" cmake --install build/sources/shiboken6

	# Install egg info
	export PATH="/usr/lib/qt6/bin:$PATH"
	local pythonpath="$(python3 -c "from sysconfig import get_path; print(get_path('platlib'))")"
	python3 setup.py egg_info --build-type=pyside6
	python3 setup.py egg_info --build-type=shiboken6
	cp -r PySide6.egg-info "$pkgdir/$pythonpath"
	cp -r shiboken6.egg-info "$pkgdir/$pythonpath"
}

pyshiboken() {
	pkgdesc="qt shiboken6 python module"

	amove usr/bin # shiboken6 build tool
	amove usr/lib/libshiboken6* # python object
	amove usr/lib/python3.*/site-packages/shiboken6*
}

pyside() {
	pkgdesc="qt pyside6 python module"

	amove usr/lib/libpyside6* # python object
	amove usr/lib/python3.*/site-packages/PySide6*
	amove usr/share/PySide6
}

sha512sums="
330eef5d63c5225e0d3ee8be1229e81ac04ec39772766fd2dfe197291a8153c00a2cd548ce6e49a0a198b3081535c79a7e649a6e9f03dfe67df68790543ea023  6.4.1-pyside6-.tar.xz
09efbc70cd203d1451fe6e901ae880241f57b742baab1a083292272da4a0d8976bff6e1e2b4c71ed4203a0b31c4f64fe4b97dd6e545faa7d2c238bf471108174  pyside6-fix-python-1.patch
17e59fea2b4f628ebbd7ea5ad59d282c616ab2a9e1f36c570113e3b6c6c6b98aaa54418904f0696f33b703eb98a87fb93dbd692155fadcc287608bc026f2b0c6  pyside6-fix-python-2.patch
"
