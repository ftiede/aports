# Contributor: Fabian Affolter <fabian@affolter-engineering.ch>
# Maintainer: Fabian Affolter <fabian@affolter-engineering.ch>
pkgname=py3-pylint
pkgver=2.15.9
pkgrel=0
pkgdesc="Analyzes Python code looking for bugs and signs of poor quality"
url="https://github.com/PyCQA/pylint"
arch="noarch !s390x" # py3-dill
license="GPL-2.0-or-later"
depends="
	py3-astroid
	py3-dill
	py3-isort
	py3-mccabe
	py3-platformdirs
	py3-tomlkit
	"
makedepends="
	py3-gpep517
	py3-installer
	py3-setuptools
	py3-wheel
	"
checkdepends="
	py3-gitpython
	py3-py
	py3-pytest
	py3-pytest-benchmark
	py3-pytest-runner
	py3-pytest-timeout
	py3-pytest-xdist
	py3-requests
	py3-typing-extensions
	"
source="$pkgname-$pkgver.tar.gz::https://github.com/PyCQA/pylint/archive/refs/tags/v$pkgver.tar.gz"
builddir="$srcdir"/pylint-$pkgver

build() {
	gpep517 build-wheel \
		--wheel-dir dist \
		--output-fd 1
}

check() {
	python3 -m venv --system-site-packages testenv
	testenv/bin/python3 -m installer dist/*.whl
	testenv/bin/python3 -m pytest --benchmark-disable -v tests
}

package() {
	python3 -m installer -d "$pkgdir" \
		dist/pylint-*.whl
}

sha512sums="
868d39011a545ffe719bd14dc4f99d59fad2487160900391cc91bec419d1c3857012b950a1bff790cdbdcb4b9120f83faf7c6b8adb1fd033db5edc54fe7816a1  py3-pylint-2.15.9.tar.gz
"
