# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=yakuake
pkgver=22.12.0
pkgrel=0
arch="all !armhf"
url="https://kde.org/applications/system/org.kde.yakuake"
pkgdesc="A drop-down terminal emulator based on KDE Konsole technology"
license="GPL-2.0-only OR GPL-3.0-only"
depends="konsole"
makedepends="extra-cmake-modules qt5-qtbase-dev qt5-qtsvg-dev qt5-qtx11extras-dev karchive-dev kconfig-dev kcoreaddons-dev kcrash-dev kdbusaddons-dev kglobalaccel-dev ki18n-dev kiconthemes-dev kio-dev knewstuff-dev knotifications-dev knotifyconfig-dev kparts-dev kwidgetsaddons-dev kwindowsystem-dev kwayland-dev samurai"
source="https://download.kde.org/stable/release-service/$pkgver/src/yakuake-$pkgver.tar.xz"
subpackages="$pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --build build --target install
}

sha512sums="
2569d67516b9e80277e37291c01ae7225ddfa04a4410ee13add03a1ae8453a431a525cdacb8e447bd32bf6b55e60f81eb49431d2c4a13d5e1e7addc65a830842  yakuake-22.12.0.tar.xz
"
