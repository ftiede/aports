# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kdenetwork-filesharing
pkgver=22.12.0
pkgrel=0
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://www.kde.org/applications/internet/"
pkgdesc="Properties dialog plugin to share a directory with the local network"
license="GPL-2.0-only OR GPL-3.0-only"
depends="samba"
makedepends="
	extra-cmake-modules
	kcompletion-dev
	kcoreaddons-dev
	kdeclarative-dev
	kdoctools-dev
	ki18n-dev
	kio-dev
	kwidgetsaddons-dev
	qt5-qtbase-dev
	samurai
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/kdenetwork-filesharing-$pkgver.tar.xz"
subpackages="$pkgname-lang"
options="!check" # No tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DSAMBA_INSTALL=OFF
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
4212f06e896c677138a27761f353938b6aaddf5fa36284c8ae5dd93fb4b538b5dc2aa54876e696be3216dfb299d313498b220711c3a9565a50fd36e042f511d7  kdenetwork-filesharing-22.12.0.tar.xz
"
